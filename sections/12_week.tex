\subsection{Module 7 - Networking}

\subsubsection{Amazon Virtual Private Cloud (VPC)}
In its most basic form, a cloud-based network is a private IP address space into which you can deploy computing resources. In AWS, this private network space is provided by an Amazon Virtual Private Cloud (Amazon VPC) component. A VPC enables you to define a virtual network in your own logically isolated area within the AWS Cloud.

A VPC can span multiple Availability Zones, and its key component types include a:

\begin{description}
	\item [Subnet] Subnets are logical network segments within your VPC. They allow you to subdivide your VPC network into smaller networks inside a single Availability Zone.  A subnet is public if it is attached to an internet gateway, or private if otherwise. 
	\item [Security Group] A security group is a set of “firewall” rules that secure instances.  They allow or block inbound and outbound traffic into an instance (stateful). 
	\item [Primary Network Interface] - An elastic network interface is a virtual network interface (NIC) that connects an instance to a network. Each instance in a VPC has a default network interface, the primary network interface, which cannot be detached from the instance.
	\item [Router] A router is a component that routes traffic within the VPC.
	\item [Internet Gateway] An internet gateway is a VPC component that allows communication between instances in a VPC and the internet. 
	\item [Virtual Private Gateway (VGW)] A virtual private gateway is the component that is defined on the AWS side of a virtual private network(VPN) connection. 
	\item [Customer Gateway] A customer gateway is a physical device or software application that is defined on the client side of a VPN connection.
\end{description}

\begin{figure}[h!]
  \center
  \includegraphics[width=0.9\textwidth]{vpc}
  \caption{AWS VPC Example}
\end{figure}

\paragraph{Characteristics}
\begin{itemize}
	\item VPCs can span multiple Availability Zones in an AWS Region.
	\item VPCs have an implicit router that routes all traffic in the VPC.
	\item VPCs have a default route table that specified the allowed routes out of the VPC. By default, this table defines a route (rule) that allows all traffic that is  for its CIDR IP address range to be routed locally.
\end{itemize}

\paragraph{Subnets}
Subnetsare used to further segment the VPC address range and to provide logical groupings for resources. For example, you can create a separate subnet for resources of different types, such as EC2 instances and database instances, or resources with different visibility (public or private). You can create up to 200 subnets per VPC.

\paragraph{Auto-assign public IP subnet feature}
After you create a subnet, you can specify whether new Amazon EC2 instances that are launched into it should automatically have a public IP address assigned to them. This is useful for a public subnet that is intended to host EC2 instances that are accessible to internet clients. Note that the assigned IP address is not an Elastic IP address and it will be released from the instance when the instance is stopped or terminated. 

\paragraph{Route Tables}
Each VPC comes with an implicit router that directs traffic between resources in each subnet and out of the subnet. The router uses a route table to determine the valid IP destinations out of a subnet and the corresponding target that is used to reach the destination.

\paragraph{Elastic Network Interfaces}
Each instance in a VPC has a default network interface, the primary network interface, which is assigned a private IPv4 address from the IPv4 address range of the VPC. You cannot detach a primary network interface from an instance.

\paragraph{DNS options for a VPC}
There are three options for a DNS in a VPC: DNS provided by AWS, your own DNS server, AWS Route 53 private hosted zones. As of December 2018, this DNS server is now called Amazon Route 53 Resolver. By default, Resolver directly answers DNS queries for domain names within the VPC, and performs recursive lookups against public name servers for all other domain names.

\subsubsection{VPC Connectivity Options}

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{vpcconnectivity}
  \caption{VPC Connectivity Options}
\end{figure}

\paragraph{Network Address Translation}
You can use a network address translation (NAT) device to enable an instance in a private subnet to connect to the internet or other AWS services, but prevent the internet from initiating connections with the instance. AWS offers two kinds of NAT devices: a NAT gateway and a NAT instance. NAT gateways are recommended because they provide better availability and bandwidth compared to NAT instances.

\paragraph{VPC Peering}
By design, VPCs are isolated network sections in the AWS Cloud. If you need to connect two VPCs together to exchange traffic or data, you can create a VPC peering connection. A VPC peering connection is a networking connection between two VPCs that enables you to route traffic between them by using private IP addresses. The VPCs can be in the same Region, in different Regions, or even in different AWS accounts.

\textbf{States of a Peering}
\begin{itemize}
	\item Initiating Request - A request for a VPC peering connection has been initiated.
	\item Pending Acceptance - The VPC peering connection request is awaiting acceptance from the owner of the accepter VPC. If no action is taken on the request, it expires after 7 days.
	\item Provisioning - The VPC peering connection request has been accepted, and will soon be in the active state. 
	\item Active - The VPC peering connection is active, and traffic can flow between the VPCs (provided that your security groups and route tables allow the flow of traffic).
\end{itemize}

\paragraph{AWS site-to-site VPN}
The AWS Site-to-Site VPN option creates an IPsec VPN connection between remote customer networks and their Amazon VPC over the internet. The resulting connection is secured using the IPsec protocol. 

A Site-to-Site VPN connection consists of the following components: Virtual private gateway, Customer Gateway.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{vpns2s}
  \caption{Site-to-Site VPN}
\end{figure}

\paragraph{AWS Direct Connect plus VPN}
Using AWS Direct Connect, you can create a physicalconnection between your data center and AWS, which creates a secure and dedicated endpoint between your internal networks and your cloud network. One end of the cable is connected to your data center router, and the other end is connected to an AWS Direct Connect router in an AWS Direct Connect location.

\paragraph{AWS VPN CloudHub}
If you have multiple VPC-to-remote-site VPN connection requirements, consider using an AWS VPN CloudHub model to implement them in a convenient and cost-effective manner. AWS VPN CloudHub operates on a simple hub-and-spoke model, and it can include both AWS Site-to-Site and AWS Direct Connect plus VPN connections. The model enables your remote sites to communicate with the VPC and also with each other.

To use the AWS VPN CloudHub, you must create a virtual private gateway with multiple customer gateways. Each customer gateway is configured to advertise its Border Gateway Patrol (BGP) routes over its Site-to-Site VPN connection.

\paragraph{VPC Endpoint}
A VPC endpoint enables you to privately connect your VPC to supported AWS services and VPC endpoint services powered by AWS PrivateLink without requiring an internet gateway, NAT device, VPN connection, or AWS Direct Connect connection. Instances in your VPC do not need to know the public IP address of the resources that they want to communicate with, and the traffic between your VPC and the service provider does not leave the Amazon network. 

\paragraph{Gateway Endpoint}
A gateway endpoint provides a mechanism to directly access resources in Amazon S3 and Amazon DynamoDB from within a VPC without needing to go through the internet. In addition, you can take advantage of this feature without needing to change any of your existing application code.

\paragraph{AWS Transit Gateway}
AWS Transit Gateway enables you to connect Amazon VPCs and on-premises networks to a single gateway. As the number of VPCs in your AWS Cloud environment grows, the requirement to interconnect them becomes more and more challenging to implement. One solution is to connect pairs of VPCs by using peering.

\subsubsection{Securing your Network}
Your virtual network canand shouldbe secured at several different levels:
\begin{enumerate}
	\item The most broadly scoped level of securityis at the route-table level. As you learned earlier, having a private subnet with no direct path to the internet is one of the best ways to protect your internal computing resources against unauthorized access.
	\item The second level isnetwork access control lists (network ACLs). Network ACLs provide the ability to define the default security behavior for your subnets. The VPC or subnet layer security is typically controlled by the network security team.
	\item Security groups can be used at the third level to control behavior at the instance or elastic network interface level.
	\item At the fourth level, you might choose to use some form of third-party, host-based detection software that monitors individual Amazon EC2 instances for specific threats (such as malware intrusion, known operating system vulnerabilities, and security auditing).
\end{enumerate}

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{netsec}
  \caption{Layered Network Defense for VPCs}
\end{figure}

\paragraph{Security Groups}
Security groups are used to restrict traffic in and out of the elastic network interfaces that are associated with them.

Some important things to note with security groups are:
\begin{itemize}
	\item A VPC has a default security group that allows all outbound traffic, but denies all inbound traffic.
	\item When you launch an instance in a VPC and do not specify a security group for it, the instance is automatically assigned to the default security group.
	\item Rules are defined in two separate inbound and outbound tables.
	\item You can specify allow rules, but not deny rules.
	\item All rules are evaluated before deciding whether to allow traffic.
	\item All outbound traffic in response to an inbound request is permitted.
\end{itemize}

Security groups are typically configured with the help the application development team because they understand which protocols and port numbers their applications use.

\paragraph{Network Access Control List}
Network access control lists (network ACLs) are associated with a subnet within a VPC. They define rules that allow or deny the inbound and outbound traffic within the subnet. hey are stateless, which means that the inbound and outbound rules are independent of each other.If a type of request traffic is allowed inbound, you must explicitly define an outbound rule for the same type to allow the response traffic.

\paragraph{Bastion Host}
A bastion host serves as a jump point from the public internet to the Amazon EC2 instances and other resources in a private subnet. By using a bastion host, you can access private resources publicly through the internet in a way that still minimizes the attack surface of your private subnet.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{bastion}
  \caption{Bastion Host with SSH Access}
\end{figure}

\subsubsection{Troubleshooting Network on AWS}

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{networktroubleshooting}
  \caption{Network Troubleshooting on AWS}
\end{figure}

\subsection{Module 9 - Monitoring and Security}
Amazon CloudWatch is a monitoring and management service. It provides you with data and insights to monitor your applications, understand and respond to system-wide performance changes, optimize resource utilization, and get a unified view of operational health. The three main features that CloudWatch supports are CloudWatch monitoring, CloudWatch logs, and CloudWatch Events.

\subsubsection{CloudWatch Monitoring}
Amazon CloudWatch is integrated with more than 70 AWS services that automatically publish detailed standard metrics without any action on your part. This feature allows you to monitor most of the resources you deploy on AWS. If you want to use CloudWatch to monitor your own applications for operational performance, troubleshooting issues, and spotting trends, you can collect custom metrics. For example, if you are running an HTTP server on an EC2 instance, you could publish a statistic on the memory usage of the httpd service.  User activity is another example of a custom metric you can collect and monitor over a period of time. 

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{cloudwatchmonitoring}
  \caption{Cloud Watch Monitoring Example}
\end{figure}

\paragraph{Metric Components}
Metricsare the fundamental concept in CloudWatch. A metric represents a time-ordered set of data points that are published to CloudWatch. Metrics are uniquely defined by a name, a namespace, and zero or more dimensions. Each data point has a time stamp, and (optionally) a unit of measure.

\begin{description}
	\item [Namespace] is a container for CloudWatch metrics. Metrics in different namespaces are isolated from each other, so that metrics from different applications are not mistakenly aggregated into the same statistics.
	\item [Dimension] is a name-value pair that uniquely identifies a metric. You can assign up to 10 dimensions to a metric. 
	\item [Period] is the length of time associated with a specific Amazon CloudWatch statistic. Periods are defined in numbers of seconds.
\end{description}

\paragraph{Standard and Custom Metrics}
Standard metrics are grouped by service. Metrics cannot be deleted, but they automatically expire after 15 months if no new data is published to them. Data points that are older than 15 months expire on a rolling basis. As new data points come in, data that is older than 15 months is dropped.

AWS services send metrics to CloudWatch, and you can publish your own metrics to CloudWatch using the AWS CLI,the application programming interface (API), or CloudWatch Agent. Custom metrics are grouped by the namespace you define when you create them. 

\paragraph{CloudWatch Alarms}
You can create a CloudWatch alarm that watches a single CloudWatch metric or the result of a math expression that is based on multiple CloudWatch metrics. The alarm performs one or more actions based on the value of the metric or expression relative to a threshold over a number of time periods. 

An alarm has three possible states:
\begin{itemize}
	\item OK
	\item ALARM
	\item INSUFFICIENT\_DATA
\end{itemize}

\paragraph{Activated Detailed Instance Monitoring}
By default, EC2 instances are enabled for basic CloudWatch monitoring, with data available in 5-minute increments as part of the AWS Free Tier.However, you can also enable detailed monitoring at an additional cost.After detailed monitoring is enabled, the monitoring data becomes available in 1-minute increments.

\subsubsection{CloudWatch Events}
Amazon CloudWatch Events deliver a near-real-time stream of system events that describe changes in AWS resources. Using simple rules that you can configure, you can match events and route them to one or more target functions or streams. You can use CloudWatch Events to schedule automated actions that self-trigger at certain times using cron or rate expressions.

\begin{description}
	\item [Events] An eventindicates a change in your AWS environment. AWS resources can generate events when their state changes.
	\item [Targets] A target processes events. Example targets include Amazon EC2 instances, AWS Lambda functions, Amazon SNS topics, and Amazon SQS queues. 
	\item [Rules] A rule matches incoming events and routes them to targets for processing. A single rule can route to multiple targets, all of which are processed in parallel. 
\end{description}

\subsubsection{CloudWatch Logs}
You can use Amazon CloudWatch Logs to monitor, store, and access your log files from Amazon EC2 instances, AWS CloudTrail, AmazonRoute 53, and other sources. You can then retrieve the associated log data from CloudWatch Logs. You can monitor your logs, in near-realtime, for specific phrases, values, or patterns. 

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{cloudwatchlogs}
  \caption{CloudWatch Logs Process}
\end{figure}

\subsubsection{CloudTrail}
AWS CloudTrail is an AWS service that generates logs of calls to the AWS API. Because the AWS API underlies both the AWS CLI and the AWS Management Console, AWS CloudTrail can record all activity against the services it monitors. In this way, it enables governance, compliance, operational auditing, and risk auditing of AWS accounts.

After AWS CloudTrail is configured, it will push the auditing logs to Amazon S3. Though CloudTrail is full-featured, it does not track events that occur within an Amazon EC2 instance.

\paragraph{Example}
By using AWS CloudTrail, you can store logs on API usage in an S3 bucket, and then later analyze those logs to answer a number of questions, such as:
\begin{itemize}
	\item Why was a long running instance terminated, and who terminated it? The answers could be useful for organizational traceability and accountability.
	\item Who changed a security group configuration? Accountability and security auditing teams might want to know this information.
	\item Is any activity coming from an unknown IP address range? Such activity could indicate a potential external attack, which is a security concern.
	\item What activities were denied due to lack of permissions? Such activity could indicate internal or external attack attempts.
\end{itemize} 

\subsubsection{Service Integration with Amazon Athena}
Amazon Athena is an interactive query service that makes it easy to analyze data in Amazon S3 using standard SQL. To use Athena, you point to your data in Amazon S3, define the schema, and start querying using standard SQL. Most results are delivered within seconds. With Athena, there’s no need for complex ETL jobs to prepare your data for analysis. This makes it easy for anyone with SQL skills to quickly analyze large-scale datasets.

\begin{enumerate}
	\item Create an Amazon S3 bucket and load data into it
	\item Define the schema (table definition) that describes the data structure
	\item Start querying data using SQL
\end{enumerate}

\subsubsection{AWS Config}
AWS Config is a service that enables you to assess, audit, and evaluate the configurations of your AWS resources. AWS Config continuously monitors and records your AWS resource configurations and allows you to automate the evaluation of recorded configurations against desired configurations.

Features:
\begin{itemize}
	\item Retrieve an inventory of AWS resources
	\item Discover new and deleted resources
	\item Record configuration changes continuously and determine your overall compliance against the configurations specified by your internal guidelines
	\item Get notified when configurations change and dive into detailed resource configuration histories
\end{itemize}

\paragraph{Config Rules}
AWS Config provides a rule system. You can use existing rules from AWS and from partners. You can also define your own custom rules by using AWS Lambda.

Exmaple Rules:
\begin{itemize}
	\item Amazon Elastic BlockStore (AmazonEBS) volumes are encrypted.
	\item Instances are being created only from approved Amazon Machine Images
	\item Elastic IP addresses are attached to instances
\end{itemize}
