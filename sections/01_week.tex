\section{Introduction}
Cloud Infrastructure is all what is needed to support the computing requirements of an application.

\subsection{Tools}

\begin{description}
  \item[Terraform] Terraform is an open-source infrastructure as code (IaaC) software tool that provides a consistent CLI workflow to manage hundreds of cloud services. Terraform codifies cloud APIs into declarative configuration files.
  \item[Ansible] Ansible is an IT automation tool. It can configure systems, deploy software, and orchestrate more advanced IT tasks such as continuous deployments or zero downtime rolling updates. Ansible’s main goals are simplicity and ease-of-use. It also has a strong focus on security and reliability, featuring a minimum of moving parts, usage of OpenSSH for transport (with other transports and pull modes as alternatives), and a language that is designed around auditability by humans–even those not familiar with the program.
  \item[Kubernetes] Kubernetes, also known as K8s, is an open-source system for automating deployment, scaling, and management of containerized applications.
  \item[Helm] Helm is a package and life cycle manager for Kubernets. Helm helps you manage Kubernetes applications — Helm Charts help you define, install, and upgrade even the most complex Kubernetes application.
  \item[Promotheus] Prometheus is an open-source systems monitoring and alerting toolkit originally built at SoundCloud. Prometheus collects and stores its metrics as time series data, i.e. metrics information is stored with the timestamp at which it was recorded, alongside optional key-value pairs called labels.
\end{description}

\subsection{DevOps}
DevOps is a culture, fostering collaboration among all roles involved in the development and maintenance of software. DevOps team would like to be continuously pushing small changes out to production, monitor what happens in production, see the result of that and use that to make further improvement to the system

\begin{description}
  \item[System Thinking] Avoid focusing on only one piece.
  \item[Feedback] Keeping production healthy is everyones job.
  \item[Learning] Continual experimentation and learning.
\end{description}

\subsubsection{DevOps Pipeline}
A DevOps pipeline is a set of practices that the development (Dev) and operations (Ops) teams implement to build, test, and deploy software faster and easier. Agile can be described as a development methodology designed to maintain productivity and drive releases with the common reality of changing needs. Although DevOps and agile are different, when used together these two methodologies lead to greater efficiency and more reliable results.

\begin{figure}[h]
  \center
  \includegraphics[width=0.5\linewidth]{devops}
  \caption{DevOps Pipeline}
\end{figure}

\begin{description}
  \item[Plan Stage] Objectives and requirements are added to the backlog. Work items are also added per feedback from end users and the operations team. Several issue board and project management tools can be used to track these activities and plan.
  \item[Code Stage] The coding happens on a shard Git repository where all the devs push their code to. The repository tool/application is called Version Control Software (VCS) or Source Code Management (SCM).
  \item[Build Stage] A continuous integration pipeline is invoked when code is pushed to the repository. Code runs through several tests to identify bugs, errors and vulnerabilities. If tests pass, code can be reviewed and merged. 
  \item[Test Stage] The build is deployed to a staging environment for testing.  Several automated tests can be performed, such as load, accessibility, performance, end-to-end, and more. User Acceptance Testing (UAT) can also be set up in this stage.
  \item[Release Stage]  In this stage, we tag a snapshot of the code with Semantic Versioning. Changes, features, breaking changes, and deprecated features are documented. Release can include artifacts such as binaries and packages. 
  \item[Deploy Stage] Install our release into the production environment. This process can be automated or manual process in the pipeline.  Several deployment strategies can be used in order to avoid downtime.
  \item[Operate Stage] Stage where engineers from the infrastructure and operations team ensure the application runs smoothly. The infrastructure can scales to meet application demand. Troubleshoot and resolve infrastructure issue. The operations team also document issues to provide feedback for next planning stage.
  \item[Monitor Stage]  Application monitoring and logging is used to collect data on the usage, performance, errors, and more. Data gathered from this stage is used to provide feedback on the following iterations of the DevOps lifecycle
\end{description}

\subsubsection{DevOps Life Cycle}
The DevOps pipeline form four different phases in the DevOps life cycle.

\begin{figure}[h]
  \center
  \includegraphics[width=0.7\linewidth]{devopslifecycle}
  \caption{DevOps Pipeline}
\end{figure}

\begin{description}
   \item[Continuous Integration] Commit early, commit often.  Push code to the repository in the smallest possible size.  Push code to the repository frequently. CI Testing in order to  Identify any security vulnerabilities, errors and bugs. Unit testing and linting should be used.  
  \item[Continuous Delivery] Continuous Delivery requires a trigger to deploy the application to the test environment. Load Performance, Functional Testing, User acceptance testing. 
  \item[Continuous Deployment] The deployment to the productive system is automated. There are different deployment strategies: Rolling deployment (in Groups), Blue-Green (two environments), Canary (Pilot group). 
  \item[Continuous Feedback] Beginning and end of the continuous lifecycle. Metrics, statistics, analytics and feedback from the customer and teams involved are taken into account to continuously improve the product.
\end{description}

\section{DevOps GitLab}
GitLab CI is a Continuous Integration server built right into GitLab. It has the capabilities to build, test and deploy automatically at every change, keep building and scripting in one repository.

\subsection{CI Modes}
The GitLab Continuous Integration can be configured in multiple ways.

\subsubsection{Automated DevOps}
The automated DevOps is a GitLab pipeline running automatically when a commit into the repository occurs. This can be used to either directly run test of the newly written code or build the application or push the changes in the IaaC directly onto the cloud or cluster.

\subsubsection{DevOps Zero Configuration}
GitLab Auto DevOps is a collection of pre-configured features and integrations that work together to support the software delivery process. Auto DevOps detects your code language and uses CI/CD templates to create and run default pipelines. All we need to kick it off is to enable it.

\subsubsection{CI Configuration}
The Continuous integration can be configure with the "gitlab-ci.yml" file. In this file we can define: which scrips we want to run, Other configuration files and templates to include, Dependencies and caches, The commands we want to execute, the location we want to deploy the application to, when we want the pipeline to run. 
 
\subsection{GitLab CI}
GitLab itself is an example of a project that uses Continuous Integration as a software development method. For every push to the project, a set of checks run against the code.

\begin{lstlisting}[caption=Simple Pipeline]
stages:
  - build
  - test

build:
  stage: build
  script:
    - echo "Building"
    - mkdir build
    - touch build/info.txt
  artifacts:
    paths:
      - build/

test:
  stage: test
  script:
    - echo "Testing"
    - test -f "build/info.txt"
\end{lstlisting}

\subsubsection{Build Stages}
Each stage contains one or more jobs. Stages are run in the order they are declared. Any job failure marks the stage as failed. When no stages are declared, the default stages are: build, test, deploy. Any unassigned job is assigned to the test stage. 

\subsubsection{Parallel Execution}
By assigning two jobs to the same stages, gitlab CI runs the two jobs in parallel together. In order to execute the exact same test multiple times the tag "parallel:" can be used. 

\begin{lstlisting}
test-01:
  stage: test
  script:
    - echo "Testing"
    - test -f "build/info.txt"

test-02:
  stage: test
  script:
    - echo "Testing"
    - test -f "build/info.txt"
\end{lstlisting}

\subsubsection{Caches}
GitLab CI jobs typically run with a clean environment every time. This can make jobs slow due to re-downloading dependencies (Exactly the same content is being downloaded every time). We can specify paths to cache the modules and to speed up future builds.

\subsubsection{Artefacts}
GitLab CI can capture artefacts from builds Downloadable from the web interface or  can be used in future job. We can also define, when the artefacts are to be created: on success (default), on failure, always. Additionally we can set an expire date for the artefacts.

By default, any artefacts produce in one job is going to be available to the other jobs.


\begin{lstlisting}
build-stage:
  stage: build
  script:
    - echo "Building"
    - mkdir build
    - touch build/info.txt
  artifacts:
    when: always
    expire_in: 1 week
    paths:
      - build/
\end{lstlisting}

\subsubsection{Variables for Builds}
In the web UI settings we can write the environment Variables for a pipeline. When a variable is missing it does not block execution.


